# Copyright 2011 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ none ] ]

SUMMARY="A tiling, keyboard driven X11 Window Manager written entirely in Common Lisp"
HOMEPAGE="https://stumpwm.github.io"

LICENCES="GPL-2"
SLOT="0"

DEPENDENCIES="
    build:
        dev-lisp/cl-ppcre
        dev-lisp/clx[~scm]
    build+run:
        dev-lang/sbcl [[ note = [ contains asdf ] ]]
    suggestion:
        (
            app-misc/rlwrap
            x11-apps/xprop
        ) [[ *description = [ For stumpish ] ]]
"

RESTRICT="strip"

DEFAULT_SRC_CONFIGURE_PARAMS=( --with-lisp=sbcl )
DEFAULT_SRC_COMPILE_PARAMS=( -j1 )

src_prepare() {
    if [[ $PV == 0.9.9 ]]; then
        # Upstream didn't change the version before packaging
        sed -i "${PN}.asd" -e 's/:version "0.9.8"/:version "0.9.9"/' || die
        # Does not build with asdf:oos, using require to load the package
        sed -i "load-${PN}.lisp.in" -e "s/asdf:oos 'asdf:load-op/require/" || die
    fi

    default
    eautoconf
}

src_install() {
    emake install destdir="${IMAGE}"
}
